import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  AppRegistry, TouchableOpacity
} from 'react-native';
import Voice from 'react-native-voice';

import Tts from 'react-native-tts';


export default class VoiceNative extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      results: [],
      diego: 0,
      filipe: 0,
      quantidadePontos: 15,
      quantidade: false,
      inverter: false
    };
    Tts.setDefaultLanguage('pt-BR');
   /* Tts.voices().then(voices => {
      
      for (let index in voices){
        if(voices[index].quality == 400 && voices[index].language == "pt-BR"){
          console.warn(voices[index].id);
        }
      }
      console.log(voices)
    
    });
*/

Tts.addEventListener('tts-start', event => console.log('start', event));
Tts.addEventListener('tts-finish',  event => this.gravar());
Tts.addEventListener('tts-cancel', event => console.log('cancel', event));
    Tts.setDefaultVoice('pt-br-x-afs-network');
Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
    Voice.onSpeechError = this.onSpeechError.bind(this);
    this.gravar()
  }
componentWillUnmount() {
    Voice.destroy().then(Voice.removeAllListeners);
  }
onSpeechStart(e) {
    this.setState({
      started: '√',
    });
  };
onSpeechRecognized(e) {
    this.setState({
      recognized: '√',
    });
  };
onSpeechResults(e) {
    this.setState({
      results: e.value,
      error: false,
      gravando: false
    });
    this.falar(e.value)
  }
  onSpeechError(e) {
    this.setState({error: true})
    this.gravar()
  }

gravar(){
  console.log('gravando');
  this._startRecognition()
}
async _startRecognition(e) {
    this.setState({
      gravando: true,
      recognized: '',
      started: '',
      results: [],
    });
    try {
      await Voice.start('pt-BR');
    } catch (e) {

      console.error(e);

    }

  console.log('chegou')
  }

  subtraiDiego(){
    let diego = this.state.diego -1;
    this.setState({diego})
  }
  subtraiFilipe(){
    let filipe = this.state.filipe -1;
    this.setState({filipe})
  }

  somaDiego(){
    let diego = this.state.diego +1;
    this.setState({diego})
  }
  somaFilipe(){
    let filipe = this.state.filipe +1;
    this.setState({filipe})
  }

  identificarNumero(valores){
    if(valores.includes('um')){
      return 1;
    }else if(valores.includes('dois')){
      return 2;
    }else if(valores.includes('tres')){
      return 3;
    }else if(valores.includes('quatro')){
      return 4;
    }else if(valores.includes('cinco')){
      return 5;
    }else if(valores.includes('seis')){
      return 6;
    }else if(valores.includes('sete')){
      return 7;
    }else if(valores.includes('oito')){
      return 8;
    }else if(valores.includes('nove')){
      return 9;
    }else if(valores.includes('dez')){
      return 10;
    }else if(valores.includes('onze')){
      return 11;
    }else if(valores.includes('doze')){
      return 12;
    }else if(valores.includes('treze')){
      return 13;
    }else if(valores.includes('quatorze')){
      return 14;
    }else if(valores.includes('quinze')){
      return 15;
    }else if(valores.includes('dezesseis')){
      return 16;
    }else if(valores.includes('dezessete')){
      return 17;
    }else if(valores.includes('dezoito')){
      return 18;
    }else if(valores.includes('dezenove')){
      return 19;
    }else if(valores.includes('vinte')){
      return 20;
    }else if(valores.includes('vinte e um') | valores.includes('vinte um')){
      return 21;
    }else if(valores.includes('vinte e dois') | valores.includes('vinte dois')){
      return 22;
    }else if(valores.includes('vinte e tres') | valores.includes('vinte tres')){
      return 23;
    }else if(valores.includes('vinte e quatro') | valores.includes('vinte quatro')){
      return 24;
    }else if(valores.includes('vinte e cinco') | valores.includes('vinte cinco')){
      return 25;
    }else if(valores.includes('vinte e seis') | valores.includes('vinte seis')){
      return 26;
    }else if(valores.includes('vinte e sete') | valores.includes('vinte sete')){
      return 27;
    }else if(valores.includes('vinte e oito') | valores.includes('vinte oito')){
      return 28;
    }else if(valores.includes('vinte e nove') | valores.includes('vinte nove')){
      return 29;
    }else if(valores.includes('trinta')){
      return 30;
    }
    
    
    else{
      return 'null';
    }
  }

  falar(valores){


    let quantidade = this.state.quantidade;

    if(quantidade == true){
      let quantidadePontos = this.identificarNumero(valores);
      if(quantidadePontos != 'null'){

        Tts.speak('Partida de'+quantidadePontos+'pontos');
        this.setState({quantidadePontos, quantidade: false})
      }else{

        Tts.speak('Número não identificado');
      }
    }else{





    if(valores.includes('ponto de Filipe') | valores.includes('ponto Filipe') | valores.includes('ponto para Filipe') | valores.includes('ponto pra Filipe') | valores.includes('para Filipe')){
      let filipe = this.state.filipe;
      let diego = this.state.diego;
      filipe += 1;
      this.setState({filipe})


      if(this.state.melhorTres == true && filipe == 2){

        Tts.speak('Vitória de filipe');
      }else if(this.state.melhorTres == false && filipe == this.state.quantidadePontos){

        Tts.speak('Vitória de filipe');
      }else{


      if(filipe == this.state.diego){
        if(filipe == 1 && this.state.melhorTres == true){

          Tts.speak('Ultimo ponto');
        }else{
        if(filipe == this.state.quantidadePontos - 1 && diego == this.state.quantidadePontos - 1){

          Tts.speak('melhor de tres');
          let diego = 0;
          let filipe = 0;
          this.setState({melhorTres: true, diego, filipe})
          }else{
              Tts.speak(filipe+'a'+this.state.diego);
          }
        }
      }else{
        if(this.state.diego == 1 && this.state.melhorTres == true){

          Tts.speak('Ponto da vitória para Diego');
        }else{
      if(filipe > this.state.diego){
        
        if(filipe == this.state.quantidadePontos - 1){
          //matchpoint
          Tts.speak('Ponto da vitória para filipe');
        }else{
          Tts.speak(filipe+'a'+this.state.diego+'para filipe');
        }
      }else{
        if(this.state.diego == this.state.quantidadePontos - 1){
          //matchpoint
          Tts.speak('Ponto da vitória para diego');
        }else{
          Tts.speak(this.state.diego+'a'+filipe+'para diego');
        }
      }
    }
    }
    }
  }else if(valores.includes('ponto de Diego') | valores.includes('ponto Diego') | valores.includes('ponto para Diego') | valores.includes('ponto pra Diego') | valores.includes('para Diego')| valores.includes('ponto pra Diego') | valores.includes('para Diego')){
      let diego = this.state.diego;
      let filipe = this.state.filipe;
      diego += 1;
      this.setState({diego})


      if(this.state.melhorTres == true && diego == 2){

        Tts.speak('Vitória de Diego');
      }else if(this.state.melhorTres == false && diego == this.state.quantidadePontos){

        Tts.speak('Vitória de Diego');
      }else{

      if(filipe == this.state.diego){
        if(this.state.diego == 1 && this.state.melhorTres == true){

          Tts.speak('Ultimo ponto');
        }else{
        if(filipe == this.state.quantidadePontos - 1 && diego == this.state.quantidadePontos - 1){

          Tts.speak('melhor de tres');

          let diego = 0;
          let filipe = 0;
          this.setState({melhorTres: true, diego, filipe})
          }else{
              Tts.speak(filipe+'a'+this.state.diego);
          }
        }
      }else{
        if(this.state.diego == 1 && this.state.melhorTres == true){

          Tts.speak('Ponto da vitória para Diego');
        }else{
      if(filipe > this.state.diego){
        if(filipe == this.state.quantidadePontos - 1){
          //matchpoint
          Tts.speak('Ponto da vitória para filipe');
        }else{
          Tts.speak(filipe+'a'+this.state.diego+'para filipe');
        }
      }else{
        if(this.state.diego == this.state.quantidadePontos - 1){
          //matchpoint
          Tts.speak('Ponto da vitória para diego');
        }else{
          Tts.speak(this.state.diego+'a'+filipe+'para diego');
        }
      }
    }
  }
    }
  }else if(valores.includes('iniciar partida')){
      let diego = 0;
      let filipe = 0;
      this.setState({diego, filipe, melhorTres: false})

      Tts.speak('Nova partida');
    }else if(valores.includes('reiniciar partida')){
      let diego = 0;
      let filipe = 0;
      this.setState({diego, filipe, melhorTres: false})

      Tts.speak('Reiniciando partida');
    }
    else if(valores.includes('inverter lados') | valores.includes('mudar lados')){
      let inverter = this.state.inverter;
      if(inverter == true){
        inverter = false;
      }else{
        inverter = true;
      }
      this.setState({inverter})

      Tts.speak('Lados invertidos');
    }else if(valores.includes('quantidade pontos') | valores.includes('alterar quantidade de pontos') | valores.includes('quantidade de pontos') | valores.includes('alterar quantidade pontos')){
      let quantidade = true;
      this.setState({quantidade})
      Tts.speak('Quantos pontos');
    }
    
    else{

      Tts.speak('Comando não reconhecido');
    }
  }
}
render () {
    return (
        <View style={{flex: 1}}>

        {this.state.error ? <View style={{height: 500, width: 500, backgroundColor: '#000'}}/> : <View/>}

        {this.state.results.map((result, index) => <Text style={{fontSize: 30, color: '#000'}}> {result}</Text>
        )}
        


        { !this.state.inverter ?
        <View style={{flexDirection: 'row', height: '100%'}}>
        
          <View style={{height: '100%', width: '50%', backgroundColor: '#e51'}}>

          <Text style={{ fontSize: 50, marginEnd: 20 ,color: '#fff', textAlign: 'center'}}>Filipe</Text>
      
          
            <View style={{flexDirection: 'row', height: '80%', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity
        style={styles.button}
        onPress={() => this.subtraiFilipe()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>-</Text>
      </TouchableOpacity>
        <Text style={{height: '100%', marginTop: 60, width: '60%', fontSize: 170, alignSelf:'center', color: '#fff', textAlign: 'center'}}>{this.state.filipe}</Text>
        <TouchableOpacity
        style={styles.button}
        onPress={() => this.somaFilipe()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>+</Text>
      </TouchableOpacity>
            </View>
          </View>


          <View style={{height: '100%', width: '50%', backgroundColor: '#39f214'}}>
          
          <Text style={{ fontSize: 50, marginEnd: 20 ,color: '#fff', textAlign: 'center'}}>Diego</Text>
      
          <View style={{flexDirection: 'row', height: '80%', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity
        style={styles.button}
        onPress={() => this.subtraiDiego()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>-</Text>
      </TouchableOpacity>
        <Text style={{height: '100%', marginTop: 60, width: '60%', fontSize: 170, alignSelf:'center', color: '#fff', textAlign: 'center'}}>{this.state.diego}</Text>
        <TouchableOpacity
        style={styles.button}
        onPress={() => this.somaDiego()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>+</Text>
      </TouchableOpacity>
            </View>
            </View>
        </View>
        :
        <View style={{flexDirection: 'row', height: '100%'}}>
        
        


          <View style={{height: '100%', width: '50%', backgroundColor: '#39f214'}}>
          
          <Text style={{ fontSize: 50, marginEnd: 20 ,color: '#fff', textAlign: 'center'}}>Diego</Text>
      
          <View style={{flexDirection: 'row', height: '80%', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity
        style={styles.button}
        onPress={() => this.subtraiDiego()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>-</Text>
      </TouchableOpacity>
        <Text style={{height: '100%',  marginTop: 60, width: '60%', fontSize: 170, alignSelf:'center', color: '#fff', textAlign: 'center'}}>{this.state.diego}</Text>
        <TouchableOpacity
        style={styles.button}
        onPress={() => this.somaDiego()}
      >
        <Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>+</Text>
      </TouchableOpacity>
            </View>
            </View>

            <View style={{height: '100%', width: '50%', backgroundColor: '#e51'}}>

<Text style={{ fontSize: 50, marginEnd: 20 ,color: '#fff', textAlign: 'center'}}>Filipe</Text>


  <View style={{flexDirection: 'row', height: '80%', width: '100%', alignItems: 'center', justifyContent: 'center'}}>
  <TouchableOpacity
style={styles.button}
onPress={() => this.subtraiFilipe()}
>
<Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>-</Text>
</TouchableOpacity>
<Text style={{height: '100%', marginTop: 60, width: '60%', alignSelf: 'center', fontSize: 170, color: '#fff', textAlign: 'center'}}>{this.state.filipe}</Text>
<TouchableOpacity
style={styles.button}
onPress={() => this.somaFilipe()}
>
<Text style={{ fontSize: 100, color: '#fff', textAlign: 'center'}}>+</Text>
</TouchableOpacity>
  </View>
</View>
        </View>
      }
      </View>
    );
  }
}
const styles = StyleSheet.create({
  transcript: {
    textAlign: 'center',
    color: '#B0171F',
    marginBottom: 1,
    top: '400%',
  },
  button:{
    alignSelf: 'center'
  }
});
AppRegistry.registerComponent('VoiceNative', () => VoiceNative);